import React from 'react';
import { SearchBar } from './Search.styles';

const Search = () => {
    return (
        <SearchBar>
            <input type='Search' />;
        </SearchBar>
    );
};

export default Search;
