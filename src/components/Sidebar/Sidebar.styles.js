import styled from 'styled-components';

export const Wrapper = styled.div`
    width: 220px;
    height: 100%;
    padding: 40px;
    display: flex;
    flex-direction: column;
    flex-shrink: 0;
    transition-duration: 0.2s;
    overflow-y: auto;
    overflow-x: hidden;

    .logo {
        color: ${({ theme }) => theme.colors.white};
        font-size: ${({ theme }) => theme.fontSize.xl};
        font-weight: ${({ theme }) => theme.fontWeight.extraBold};
        position: sticky;
        top: 0;
        background-color: ${({ theme }) => theme.colors.darkBlue};
    }
`;
export const ContentMenu = styled.div`
    display: flex;
    flex-direction: column;
    a {
        color: ${({ theme }) => theme.colors.white};
        font-size: ${({ theme }) => theme.fontSize.m};
        text-decoration: none;
        display: flex;
        align-items: center;
        margin-top: 25px;
    }
    svg {
        width: 30px;
        padding: 5px;
        margin-right: 10px;
    }
`;
