import Menu from 'components/Menu/Menu';
import React from 'react';
import { ContentMenu, Wrapper } from './Sidebar.styles';

const Sidebar = () => {
    return (
        <Wrapper>
            <span className='logo'>LoveIsland</span>
            <ContentMenu>
                <Menu />
            </ContentMenu>
        </Wrapper>
    );
};

export default Sidebar;
