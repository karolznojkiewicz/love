import ProfilUser from 'components/ProfilUser/ProfilUser';
import Search from 'components/Search/Search';
import Sidebar from 'components/Sidebar/Sidebar';
import React from 'react';
import { Box, Content, Header, WrapperContainer } from './Layout.styles';

const Layout = ({ children }) => {
    return (
        <WrapperContainer>
            <Sidebar />
            <Content>
                <Header>
                    <Search />
                    <Box>
                        <ProfilUser />
                    </Box>
                </Header>
                {children}
            </Content>
        </WrapperContainer>
    );
};

export default Layout;
