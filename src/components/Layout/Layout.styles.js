import styled from 'styled-components';

export const WrapperContainer = styled.div`
    background-color: ${({ theme }) => theme.colors.darkBlue};
    max-width: 1480px;
    min-height: 300px;
    max-height: 900px;
    height: 95vh;
    width: 100%;
    display: flex;
    overflow: hidden;
    border-radius: 20px;
    font-size: ${({ theme }) => theme.fontSize.m};
    box-shadow: 0 20px 50px rgba(0, 0, 0, 0.3);
    position: relative;
`;
export const Content = styled.div`
    display: flex;
    flex-direction: column;
    flex-grow: 1;
`;
export const Header = styled.div`
    display: flex;
    align-items: flex-start;
    flex-shrink: 0;
    padding: 35px;
    justify-content: space-between;
`;
export const Box = styled.div`
    display: flex;
    align-items: center;
`;
