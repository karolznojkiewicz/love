import React from 'react';
import styled from 'styled-components';

const LogoStyles = styled.h1`
    color: ${({ theme }) => theme.colors.white};
    font-size: 3em;
    font-weight: ${({ theme }) => theme.fontWeight.bold};
    margin-bottom: 30px;
`;

const Logo = () => {
    return <LogoStyles>LoveIsland</LogoStyles>;
};

export default Logo;
