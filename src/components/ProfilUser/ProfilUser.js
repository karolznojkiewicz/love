import React, { useState } from 'react';
import { Wrapper, ProfilMenu } from './ProfilUser.styles';
import { FiChevronDown, FiChevronUp } from 'react-icons/fi';
import { logout, selectCurrentUser } from 'features/authSlice';
import { useDispatch, useSelector } from 'react-redux';
// import { UserContext } from 'UserProvider';
// import { auth } from '../../Firebase/Firebase';

const ProfilUser = () => {
    const [menu, setMenu] = useState(false);
    const dispatch = useDispatch();
    const currentUser = useSelector(selectCurrentUser);

    // const user = useContext(UserContext);
    // const { photoURL, displayName } = user;
    return (
        <Wrapper>
            <img
                class='user-img'
                src={currentUser.photoURL}
                alt={currentUser.UserName}
            />
            <span onClick={() => setMenu(!menu)}>{currentUser.UserName}</span>
            {menu ? <FiChevronDown /> : <FiChevronUp />}
            {menu && (
                <>
                    <ProfilMenu>
                        <li>Wyświetl profil</li>
                        <li>Edytuj profil</li>
                        <li>Ustawienia</li>
                        <li>Płatność</li>
                        <li
                            onClick={() => {
                                dispatch(logout());
                            }}
                        >
                            Wyloguj
                        </li>
                    </ProfilMenu>
                </>
            )}
        </Wrapper>
    );
};

export default ProfilUser;
