import styled from 'styled-components';

export const Wrapper = styled.div`
    color: ${({ theme }) => theme.colors.white};
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    padding-left: 20px;
    flex-shrink: 0;
    margin-left: auto;

    width: 200px;
    padding: 0 10px;

    img {
        width: 32px;
        height: 32px;
        border-radius: 50%;
        object-fit: cover;
    }
    span {
        font-size: ${({ theme }) => theme.fontSize.s};
        max-width: 120px;
        font-weight: ${({ theme }) => theme.fontWeight.bold};
        padding-left: 10px;
        line-height: 1.3em;
        cursor: pointer;
    }
`;
export const ProfilMenu = styled.ul`
    color: ${({ theme }) => theme.colors.darkBlue};

    width: 100%;
    list-style: none;
    display: flex;
    flex-direction: column;
    background-color: ${({ theme }) => theme.colors.white};
    padding: 5px 15px;
    margin-top: 20px;
    transition: all 0.3s ease-in-out;
    border-radius: 10px;
    li {
        padding: 10px 5px;
        cursor: pointer;
    }
`;
