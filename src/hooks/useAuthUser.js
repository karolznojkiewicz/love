import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { login, logout, selectCurrentUser } from 'features/authSlice';
import { auth } from '../Firebase/Firebase';

const useAutuser = () => {
    const dispatch = useDispatch();
    const currentUser = useSelector(selectCurrentUser);

    useEffect(() => {
        const setUser = (user) => {
            if (user) {
                console.log(user);
                dispatch(
                    login({
                        uid: user.uid,
                        UserName: user.displayName,
                        photoURL: user.photoURL,
                    }),
                );
            } else {
                dispatch(logout());
            }
        };

        const unsubscribe = auth().onAuthStateChanged(setUser);

        return () => unsubscribe();
    }, [dispatch]);

    return currentUser;
};
export default useAutuser;
