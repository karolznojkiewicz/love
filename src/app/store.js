import { configureStore } from '@reduxjs/toolkit';
import authReducer from 'features/authSlice';

export default configureStore(
    {
        reducer: {
            auth: authReducer,
        },
    },
    window.__REDUX_DEVTOOLS_EXTENSION__ &&
        window.__REDUX_DEVTOOLS_EXTENSION__(),
);
