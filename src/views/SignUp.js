import React, { useState } from 'react';
import styled from 'styled-components';
import { CgLogIn } from 'react-icons/cg';
import { RiFacebookFill, RiGoogleFill } from 'react-icons/ri';
import Logo from 'components/Logo/Logo';
import { Link } from 'react-router-dom';

const Form = styled.form`
    display: flex;
    flex-direction: column;
    color: ${({ theme }) => theme.colors.white};
    width: 500px;
    margin-top: 100px;
    input:not([type='submit']) {
        color: ${({ theme }) => theme.colors.white};
        padding: 5px 0;
        margin: 30px 0;
        border: none;
        border-bottom: 1px solid ${({ theme }) => theme.colors.white};
        background-color: transparent;
        width: 100%;
    }
    button {
        color: ${({ theme }) => theme.colors.white};
        font-size: ${({ theme }) => theme.fontSize.xl};
        background-color: transparent;
        border: 1px solid ${({ theme }) => theme.colors.white};
        border-radius: 50%;
        display: flex;
        justify-content: center;
        align-items: center;
        padding: 20px;
    }
    .form_log {
        display: flex;
        justify-content: space-between;
        a {
            color: ${({ theme }) => theme.colors.white};
            text-decoration: none;
        }
    }
    h1,
    span {
        text-align: center;
    }
    .form_span {
        margin-top: 40px;
        font-size: ${({ theme }) => theme.fontSize.l};
    }
`;
const Social = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-top: 70px;
    a {
        text-decoration: none;
        color: ${({ theme }) => theme.colors.white};
    }
`;
const Facebok = styled(Link)`
    text-decoration: none;
    font-size: ${({ theme }) => theme.fontSize.l};
    color: ${({ theme }) => theme.colors.white};
    display: flex;
    justify-content: space-between;
    background-color: ${({ theme }) => theme.colors.socials.facebook};
    padding: 15px;
    :hover {
        background-color: ${({ theme }) => theme.colors.socials.facebookLight};
    }
    span {
        padding: 0 10px;
        cursor: pointer;
    }
`;
const Google = styled(Link)`
    text-decoration: none;
    font-size: ${({ theme }) => theme.fontSize.l};
    color: ${({ theme }) => theme.colors.white};
    display: flex;
    justify-content: space-between;
    background-color: ${({ theme }) => theme.colors.socials.googleLight};
    padding: 15px;
    :hover {
        background-color: ${({ theme }) => theme.colors.socials.googleLight};
    }
    span {
        padding: 0 10px;
        cursor: pointer;
    }
`;
const ErrorAlert = styled.div`
    text-align: center;
    font-weight: ${({ theme }) => theme.fontWeight.bold};
    max-width: 500px;
    background-color: ${({ theme }) => theme.colors.orange};
    color: ${({ theme }) => theme.colors.white};
    border-radius: 5px;
    padding: 15px;
    margin-bottom: 40px;
`;
const Rejest = styled.div`
    text-align: center;
    margin-top: 40px;
    a {
        color: ${({ theme }) => theme.colors.orange};
        text-decoration: none;
        padding-left: 10px;
    }
`;

const SignUp = () => {
    const [error, setError] = useState(null);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const handleChange = (e) => {
        const { name, value } = e.currentTarget;

        if (name === 'user_email') {
            setEmail(value);
        } else if (name === 'user_password') {
            setPassword(value);
        }
    };
    return (
        <Form>
            {error && <ErrorAlert>{error}</ErrorAlert>}
            <Logo />
            <div>
                <input
                    label='user_name'
                    name='user_name'
                    placeholder='Imię'
                    value={email}
                    onChange={(e) => handleChange(e)}
                />
                <input
                    label='user_email'
                    name='user_email'
                    placeholder='user_email'
                    value={email}
                    onChange={(e) => handleChange(e)}
                />
                <input
                    type='user_password'
                    label='user_password'
                    name='user_password'
                    placeholder='hasło'
                    value={password}
                    onChange={(e) => handleChange(e)}
                />
            </div>
            <div className='form_log'>
                <Link to='passwordReset'>Nie pamiętam hasła</Link>
                <button>
                    <CgLogIn />
                </button>
            </div>

            <Social>
                <Facebok to='facebook'>
                    <RiFacebookFill />
                    <span>Zarejestruj się</span>
                </Facebok>
                <Google to='google'>
                    <RiGoogleFill />
                    <span>Zarejestruj się</span>
                </Google>
            </Social>

            <Rejest>
                <span>Masz już konto?</span>
                <Link to='/' className='center'>
                    Zaloguj się
                </Link>
            </Rejest>
        </Form>
    );
};

export default SignUp;
