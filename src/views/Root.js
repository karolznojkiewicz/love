import React from 'react';
import { GlobalStyle } from 'assets/styles/globalStyles';
import { theme } from 'assets/styles/theme';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect,
} from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import Home from './Home';
import PasswordReset from './PasswordReset';
import People from './People';
import SignIn from './SignIn';
import SignUp from './SignUp';
import Layout from 'components/Layout/Layout';
import useAuthUser from 'hooks/useAuthUser';

const Root = () => {
    const currentUser = useAuthUser();

    const PrivateRoute = ({ children, ...rest }) => {
        return (
            <Route
                {...rest}
                render={({ location }) =>
                    currentUser ? (
                        children
                    ) : (
                        <Redirect
                            to={{
                                pathname: '/signin',
                                state: { from: location },
                            }}
                        />
                    )
                }
            />
        );
    };

    return (
        <>
            <Router>
                <ThemeProvider theme={theme}>
                    <GlobalStyle />
                    <Switch>
                        <Route path='/signin'>
                            <SignIn />
                        </Route>
                        <Route path='/signup'>
                            <SignUp />
                        </Route>
                        <Route path='/passwordReset'>
                            <PasswordReset />
                        </Route>
                        <PrivateRoute path='/'>
                            <Layout>
                                <Home />
                            </Layout>
                        </PrivateRoute>
                        <PrivateRoute path='/people'>
                            <Layout>
                                <People />
                            </Layout>
                        </PrivateRoute>
                    </Switch>
                </ThemeProvider>
            </Router>
        </>
    );
};

export default Root;
