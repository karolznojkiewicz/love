export const theme = {
    colors: {
        white: '#fff',
        black: '#000',
        grey: '#e5e5e5',
        darkBlue: '#14213d',
        orange: '#fca311',

        socials: {
            facebook: '#2f55a4',
            facebookLight: '#406cc8',
            google: '#4285F4',
            googleLight: '#DB4437',
        },
    },
    fontSize: {
        s: '15px',
        m: '16px',
        l: '18px',
        xl: '20px',
    },
    fontWeight: {
        light: '300',
        medium: '400',
        bold: '600',
        extraBold: '700',
    },
};
